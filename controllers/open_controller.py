from custom_widgets import OpenWidget
from models import OpenModel

class OpenController:
    def __init__(self, view: OpenWidget, model: OpenModel) -> None:
        self.view = view
        self.model = model

        self._connect_signals()

    def _connect_signals(self):
        self.view._btn_pick_images.clicked.connect(lambda: print("Pick Images"))
        self.view._btn_add_shapes.clicked.connect(lambda: print("Add Shape"))
        self.view._btn_dialog.accepted.connect(lambda: print("Accepted"))