from PyQt5.QtWidgets import (
    QApplication,
    QFileDialog,
    QMainWindow,
    QGraphicsScene,
    QPushButton,
    QGraphicsView,
    QGraphicsItem
)
from PyQt5.QtGui import QBrush, QPen, QFont, QPixmap, QKeyEvent
from PyQt5.QtCore import Qt, QEvent
import sys


class View(QGraphicsView):
    def __init__ (self, scene: QGraphicsScene, parent=None):
        super().__init__(scene, parent)
        # self.installEventFilter(self)

    # def wheelEvent(self, event):
    #     if(event.modifiers() == Qt.ControlModifier):
    #         print("CTRL")
    #         event.ignore()
    #         return
    #     # Zoom Factor
    #     zoomInFactor = 1.25
    #     zoomOutFactor = 1 / zoomInFactor

    #     # Set Anchors
    #     self.setTransformationAnchor(QGraphicsView.NoAnchor)
    #     self.setResizeAnchor(QGraphicsView.NoAnchor)

    #     # Save the scene pos
    #     oldPos = self.mapToScene(event.pos())

    #     # Zoom
    #     if event.angleDelta().y() > 0:
    #         zoomFactor = zoomInFactor
    #     else:
    #         zoomFactor = zoomOutFactor
    #     self.scale(zoomFactor, zoomFactor)

    #     # Get the new position
    #     newPos = self.mapToScene(event.pos())

    #     # Move scene to old position
    #     delta = newPos - oldPos
    #     self.translate(delta.x(), delta.y())

    def zoom_in(self):
        zoomInFactor = 1.25
        mouse_pos = self.cursor().pos()
        mouse_pos = self.mapFromGlobal(mouse_pos)

        # Set Anchors
        self.setTransformationAnchor(QGraphicsView.NoAnchor)
        self.setResizeAnchor(QGraphicsView.NoAnchor)

        # Save the scene pos
        oldPos = self.mapToScene(mouse_pos)

        zoomFactor = zoomInFactor

        self.scale(zoomFactor, zoomFactor)

        # Get the new position
        newPos = self.mapToScene(mouse_pos)

        # Move scene to old position
        delta = newPos - oldPos
        self.translate(delta.x(), delta.y())

    def zoom_out(self):
        zoomInFactor = 1.25
        zoomOutFactor = 1 / zoomInFactor
        mouse_pos = self.cursor().pos()
        mouse_pos = self.mapFromGlobal(mouse_pos)

        # Set Anchors
        self.setTransformationAnchor(QGraphicsView.NoAnchor)
        self.setResizeAnchor(QGraphicsView.NoAnchor)

        # Save the scene pos
        oldPos = self.mapToScene(mouse_pos)

        zoomFactor = zoomOutFactor

        self.scale(zoomFactor, zoomFactor)

        # Get the new position
        newPos = self.mapToScene(mouse_pos)

        # Move scene to old position
        delta = newPos - oldPos
        self.translate(delta.x(), delta.y())

    def zoom_reset(self):
        self.resetTransform()

    def next(self):
        print("Not implemented!")
        transform = self.transform()
        print(transform.m11())
        print(transform.m12())
        print(transform.m13())
        print(transform.m21())
        print(transform.m22())
        print(transform.m23())
        print(transform.m31())
        print(transform.m32())
        print(transform.m33())

        self.resetTransform()

    def prev(self):
        print("Not implemented!")

    def save(self):
        dia = QFileDialog.getSaveFileName(caption="Save File", filter="PNG File (*.png)")

        # self.grab().save("testy.png")

    def save_all(self):
        dia = QFileDialog.getExistingDirectory(caption="Choose Save Directory")

    # def keyPressEvent(self, event: QKeyEvent) -> None:
    #     print("keyyyyy")
    #     print(event.key())
    #     if(event.modifiers() == Qt.ControlModifier and event.key() == Qt.Key.Key_Plus):
    #         print("CTRL+keyyy")
    #         return

    # def eventFilter(self, obj, event):
    #     if obj is self and event.type() == QEvent.KeyPress:
    #         if isKeyPressed("return") and isKeyPressed("shift"):
    #             self.editorBox.insertPlainText("my text to insert")
    #             return True
    #     return super(App, self).eventFilter(obj, event)