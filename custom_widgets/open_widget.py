from PyQt5.QtWidgets import (
    QDialog,
    QDialogButtonBox,
    QVBoxLayout,
    QLabel,
    QLineEdit,
    QPushButton,
    QGridLayout,
    QListWidget,
    QSizePolicy,
    QTreeWidget
)

from custom_widgets.line_widget import QHLine


class OpenWidget(QDialog):
    def __init__(self, parent) -> None:
        super().__init__(parent=parent)
        self.setWindowTitle("HELLO!")

        self._ledit_pick_images: QLineEdit
        self._btn_pick_images: QPushButton
        self._ledit_images_name: QLineEdit
        self._list_pick_shapes: QListWidget
        self._btn_del_shapes: QPushButton
        self._btn_add_shapes: QPushButton
        self._btn_dialog: QDialogButtonBox
        self._tree_selector: QTreeWidget

        self._create_ui()

    def _create_ui(self):
        root_layout = QVBoxLayout(self)

        layout_top = QGridLayout()
        
        lbl_pick_images = QLabel("Image(s) Dir:", self)
        self._ledit_pick_images = QLineEdit(self)
        self._btn_pick_images = QPushButton("Choose", self)

        lbl_images_name = QLabel("Image(s) Name:", self)
        self._ledit_images_name = QLineEdit(self)

        layout_top.addWidget(lbl_pick_images, 0, 0)
        layout_top.addWidget(self._ledit_pick_images, 0, 1)
        layout_top.addWidget(self._btn_pick_images, 0, 2)
        layout_top.addWidget(lbl_images_name, 1, 0)
        layout_top.addWidget(self._ledit_images_name, 1, 1)

        layout_top.addWidget(QHLine(), 2, 0, 1, 3)

        lbl_pick_shapes = QLabel("Shape(s) Path:", self)
        self._list_pick_shapes = QListWidget(self)
        layout_shapes_btns = QVBoxLayout()
        self._btn_del_shapes = QPushButton("Delete", self)
        self._btn_add_shapes = QPushButton("Add", self)
        layout_shapes_btns.addWidget(self._btn_add_shapes)
        layout_shapes_btns.addWidget(self._btn_del_shapes)

        self._list_pick_shapes.setMaximumHeight(layout_shapes_btns.sizeHint().height())
        self._list_pick_shapes.setSizePolicy(QSizePolicy.Policy.Preferred, QSizePolicy.Policy.Fixed)

        layout_top.addWidget(lbl_pick_shapes, 3, 0)
        layout_top.addWidget(self._list_pick_shapes, 3, 1)
        layout_top.addLayout(layout_shapes_btns, 3, 2)

        root_layout.addLayout(layout_top)

        style_btn = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        self._btn_dialog = QDialogButtonBox(style_btn, self)

        self._tree_selector = QTreeWidget(self)
        root_layout.addWidget(self._tree_selector)

        root_layout.addStretch(1)

        root_layout.addWidget(self._btn_dialog)
