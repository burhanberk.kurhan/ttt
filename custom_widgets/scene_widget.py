from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QGraphicsScene,
    QPushButton,
    QGraphicsView,
    QGraphicsItem
)
from PyQt5.QtGui import QBrush, QPen, QFont, QPixmap
from PyQt5.QtCore import Qt
import sys


class Scene(QGraphicsScene):
    def __init__(self, parent):
        super().__init__(parent)
        
 