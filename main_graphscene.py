from PyQt5.QtWidgets import QApplication, QDialog, QMainWindow, QGraphicsScene, QPushButton , QGraphicsView, QGraphicsItem, QWidget, QVBoxLayout, QToolBar, QAction
from PyQt5.QtGui import QBrush, QPen, QFont, QPixmap, QKeySequence
from PyQt5.QtCore import Qt
import sys

from custom_widgets.view_widget import View
from custom_widgets import OpenWidget
from models import OpenModel
from controllers import OpenController
 
 
class Window(QMainWindow):
    def __init__(self):
        super().__init__()
 
        self.setWindowTitle("Pyside2 QGraphic View")
        # self.setGeometry(0,0,1920,1080)
 
        self.create_ui()
 
 
        self.show()

    def create_toolbar(self):
        tool_bar = QToolBar("ToolBar", self)
        self.addToolBar(tool_bar)

        actn_zoom_in = QAction(self)
        actn_zoom_in.setText("&Zoom In")
        actn_zoom_in.setShortcut(QKeySequence('Ctrl++'))
        actn_zoom_in.triggered.connect(self.view.zoom_in)
        tool_bar.addAction(actn_zoom_in)

        actn_zoom_out = QAction(self)
        actn_zoom_out.setText("&Zoom Out")
        actn_zoom_out.setShortcut(QKeySequence('Ctrl+-'))
        actn_zoom_out.triggered.connect(self.view.zoom_out)
        tool_bar.addAction(actn_zoom_out)

        actn_zoom_reset = QAction(self)
        actn_zoom_reset.setText("&Zoom Reset")
        actn_zoom_reset.setShortcut(QKeySequence('Ctrl+0'))
        actn_zoom_reset.triggered.connect(self.view.zoom_reset)
        tool_bar.addAction(actn_zoom_reset)

        actn_frame_next = QAction(self)
        actn_frame_next.setText("&Next")
        actn_frame_next.setShortcut(QKeySequence(Qt.Key.Key_Right))
        actn_frame_next.triggered.connect(self.view.next)
        tool_bar.addAction(actn_frame_next)

        actn_frame_prev = QAction(self)
        actn_frame_prev.setText("&Previous")
        actn_frame_prev.setShortcut(QKeySequence(Qt.Key.Key_Left))
        actn_frame_prev.triggered.connect(self.view.prev)
        tool_bar.addAction(actn_frame_prev)

    def create_menubar(self):
        menu_file = self.menuBar().addMenu('&File')
        actn_open = menu_file.addAction('&Open')
        actn_open.setShortcut(QKeySequence('ctrl+o'))

        menu_file.addSeparator()
        actn_frame_save = menu_file.addAction('&Save')
        actn_frame_save.setShortcut(QKeySequence('ctrl+s'))
        actn_frame_save_all = menu_file.addAction('&Save All')
        actn_frame_save_all.setShortcut(QKeySequence('ctrl+shift+s'))
        menu_file.addSeparator()
        act_exit = menu_file.addAction('&Exit')

        menu_help = self.menuBar().addMenu('&Help')
        act_about = menu_help.addAction('&About')

        actn_open.triggered.connect(self.on_actn_open_trigerred)
        actn_frame_save.triggered.connect(self.view.save)
        actn_frame_save_all.triggered.connect(self.view.save_all)

    def on_actn_open_trigerred(self):
        print("OPEN")
        model = OpenModel()
        dlg = OpenWidget(self)
        controller = OpenController(dlg, model)
        dlg.open()


 
    def create_ui(self):
        self.center_widget = QWidget(self)
        self.layout = QVBoxLayout(self.center_widget)
        self.setCentralWidget(self.center_widget)

 
        button =QPushButton("Rotate - ", self)
        button.setGeometry(200,450, 100,50)
        button.clicked.connect(self.rotate_minus)
 
        button2 = QPushButton("Rotate + ", self)
        button2.setGeometry(320, 450, 100, 50)
        button2.clicked.connect(self.rotate_plus)
 
        scene = QGraphicsScene(self)
 
        greenBrush = QBrush(Qt.green)
        blueBrush = QBrush(Qt.blue)
 
        blackPen = QPen(Qt.black)
        blackPen.setWidth(5)
 
        pm = QPixmap("bg.png")
        scene.addPixmap(pm)
        ellipse = scene.addEllipse(0,0, 200,200, blackPen)
 
        rect = scene.addRect(0,0, 200,200, blackPen, blueBrush)
        c = rect.boundingRect().center()
        rect.moveBy(-100, -100)
        rect.setRotation
 
        scene.addText("Codeloop.org", QFont("Sanserif", 15))
 
 
        ellipse.setFlag(QGraphicsItem.ItemIsMovable)
        rect.setFlag(QGraphicsItem.ItemIsMovable)
 
        self.view = View(scene, self)
        
        # self.view.setGeometry(50,50,1920, 1080)
        self.layout.addWidget(self.view)
        self.create_toolbar()
        self.create_menubar()


    def callback_wheel(self, event):
        print("kfja")
 
 
 
    def rotate_minus(self):
        self.view.rotate(-14)
 
    def rotate_plus(self):
        self.view.rotate(14)

 
if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window()
    sys.exit(app.exec())

